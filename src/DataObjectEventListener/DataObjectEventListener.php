<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\DataObjectEventListener;

use Exception;
use Fittinq\Pimcore\Commands\Exception\NotAConcreteObjectException;
use Fittinq\Pimcore\Versioning\Versioning\ChangedFieldsExtractor;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Concrete;
use stdClass;

class DataObjectEventListener
{
    private RabbitMQ $rabbitMQ;
    private ChangedFieldsExtractor $changedFieldExtractor;
    /**
     * @var string[]
     */
    private array $commandTriggers;

    public function __construct(RabbitMQ $rabbitMQ)
    {
        $this->rabbitMQ = $rabbitMQ;
        $this->changedFieldExtractor = new ChangedFieldsExtractor();
        $this->commandTriggers = [];
    }

    /**
     * @param string[] $triggers
     * @internal Note that this should never be called in code, only in services.yaml.
     */
    public function addCommandTriggers(array $triggers, string $commandType): void
    {
        $commentEvent = new stdClass();
        $commentEvent->triggers = $triggers;
        $commentEvent->commandType = $commandType;
        $this->commandTriggers[] = $commentEvent;
    }

    /**
     * @throws Exception
     */
    public function onPostUpdate(DataObjectEvent $event): void
    {
        $object = $event->getObject();

        if (!$object instanceof Concrete) {
            throw new NotAConcreteObjectException();
        }

        $changedFields = $this->changedFieldExtractor->getLatestChanges($object);

        foreach ($this->commandTriggers as $commandTrigger) {
            if ($this->oneOrMoreTriggersMatchChangedFields($commandTrigger->triggers, $changedFields)) {
                $message = new stdClass();
                $message->objectId = $object->getId();
                $message->commandType = $commandTrigger->commandType;
                $exchange = $this->rabbitMQ->getExchange('pim.command');
                $exchange->produce($message);
            }
        }
    }

    /**
     * @param string[] $triggers
     * @param string[] $changedFields
     */
    private function oneOrMoreTriggersMatchChangedFields(array $triggers, array $changedFields): bool
    {
        return count(array_intersect($triggers, $changedFields)) > 0;
    }
}