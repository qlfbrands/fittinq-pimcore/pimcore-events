<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\DataObjectCommandConsumer;

use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;

abstract class CommandHandler
{
    private LockFactory $lockFactory;

    public function __construct(LockFactory $lockFactory)
    {
        $this->lockFactory = $lockFactory;
    }

    public function handle(int $objectId): void
    {
        $lock = $this->acquire($objectId);
        $this->execute($objectId);
        $lock->release();
    }

    public abstract function execute(int $objectId): void;

    public function acquire(int $id): LockInterface
    {
        $lock = $this->lockFactory->createLock("dataObject_$id");
        $lock->acquire(true);

        return $lock;
    }
}