<?php declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\DataObjectEventListener;

use Exception;
use Fittinq\Pimcore\Commands\DataObjectEventListener\DataObjectEventListener;
use Fittinq\Pimcore\Commands\Exception\NotAConcreteObjectException;
use PHPUnit\Framework\TestCase;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Folder;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPChannelMock;

class DataObjectEventListenerTest extends TestCase
{
    private DataObjectEventListener $dataObjectEventListener;
    private Configuration $configuration;
    private AMQPChannelMock $channel;

    protected function setUp(): void
    {
        $this->configuration = new Configuration();
        $this->dataObjectEventListener = $this->configuration->configure();
        $this->channel = $this->configuration->getChannel();
    }

    /**
     * @throws Exception
     */
    public function test_dataObjectsThatAreNotConcreteShouldThrowNotAConcreteObjectException()
    {
        $nonConcreteObject = new Folder();
        $this->expectException(NotAConcreteObjectException::class);

        $event = new DataObjectEvent($nonConcreteObject);
        $this->dataObjectEventListener->onPostUpdate($event);
    }

    /**
     * @throws Exception
     */
    public function test_changingFieldShouldTriggerMatchingEvents(): void
    {
        $testObject = $this->configuration->setUpTestObject();
        $testObject->setInput('change_value');
        $testObject->save();

        $this->dataObjectEventListener->addCommandTriggers(['TestObject.input'], 'product.translate.nl_NL');
        $message = $this->configuration->setUpMessage($testObject, 'product.translate.nl_NL');

        $event = new DataObjectEvent($testObject);
        $this->dataObjectEventListener->onPostUpdate($event);

        $this->channel->expectMessageToBeSentToExchange('pim.command', $message, '');
    }

    /**
     * @throws Exception
     */
    public function test_changingFieldShouldNotTriggerNonMatchingEvents(): void
    {
        $testObject = $this->configuration->setUpTestObject();
        $testObject->setInput('change_value');
        $testObject->save();

        $this->dataObjectEventListener->addCommandTriggers(['TestObject.textArea'], 'product.translate.nl_NL');

        $event = new DataObjectEvent($testObject);
        $this->dataObjectEventListener->onPostUpdate($event);

        $this->channel->expectNoMessageToBeSentToExchange('pim.event');
    }

    /**
     * @throws Exception
     */
    public function test_multipleEventsShouldBeTriggeredIfMultipleMatch(): void
    {
        $testObject = $this->configuration->setUpTestObject();
        $testObject->setInput('change_value');
        $testObject->setTextArea('change_value');
        $testObject->save();

        $this->dataObjectEventListener->addCommandTriggers(['TestObject.input'], 'product.translate.input.nl_NL');
        $this->dataObjectEventListener->addCommandTriggers(['TestObject.textArea'], 'product.translate.textArea.nl_NL');

        $message1 = $this->configuration->setUpMessage($testObject, 'product.translate.input.nl_NL');
        $message2 = $this->configuration->setUpMessage($testObject, 'product.translate.textArea.nl_NL');

        $event = new DataObjectEvent($testObject);
        $this->dataObjectEventListener->onPostUpdate($event);

        $this->channel->expectMessageToBeSentToExchange('pim.command', $message1, '');
        $this->channel->expectMessageToBeSentToExchange('pim.command', $message2, '', 1);
    }
}